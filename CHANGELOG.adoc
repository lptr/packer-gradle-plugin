= Changelog

== v0.2

// tag::changelog[]
* Upgraded to Grolifant 0.5.1 to simplify code base.
* Set Packer 1.1.1 as default version.
// end::changelog[]

== v0.1.3

=== Bugs

* https://gitlab.com/ysb33rOrg/packer-gradle-plugin/issues/2[#2] - Allow `packer` environment to be set.

== v0.1.2

This was an intermediate release to fix the building of documentaion.

== v0.1.1

=== Bugs

* https://gitlab.com/ysb33rOrg/packer-gradle-plugin/issues/3[#3] - When passing `-var` to packer it is not recognized.

== v0.1

First release:

