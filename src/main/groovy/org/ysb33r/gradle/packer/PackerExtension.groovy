//
// ============================================================================
// (C) Copyright Schalk W. Cronje 2017
//
// This software is licensed under the Apache License 2.0
// See http://www.apache.org/licenses/LICENSE-2.0 for license details
//
// Unless required by applicable law or agreed to in writing, software distributed under the License is
// distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and limitations under the License.
//
// ============================================================================
//

package org.ysb33r.gradle.packer

import groovy.transform.CompileStatic
import org.gradle.api.Project
import org.gradle.api.Task
import org.ysb33r.grolifant.api.exec.AbstractToolExtension
import org.ysb33r.grolifant.api.exec.ResolvableExecutable
import org.ysb33r.grolifant.api.exec.ResolveExecutableByVersion
import org.ysb33r.gradle.packer.internal.Downloader

/** Configure project defaults or task specifics for {@code Packer}.
 *
 * This also allows the {@code packer} executable to be set
 *
 * It can be passed by a single map option.
 *
 * <code>
 *   // By tag (Gradle will download and cache the correct distribution).
 *   executable tag : '7.10.0'
 *
 *   // By a physical path (
 *   executable path : '/path/to/packer'
 *
 *   // By using searchPath (will attempt to locate in search path).
 *   executable searchPath()
 * </code>
 *
 * If this build runs on a platform that supports downloading of the {@code packer} executable
 * the default will be to use the version as specified by {@link PackerExtension#PACKER_DEFAULT},
 * otherwise it will be in search mode.
 *
 * @since 0.1
 */
@CompileStatic
class PackerExtension extends AbstractToolExtension {

    /** The standard extension name.
     *
     */
    static final String NAME = 'packer'

    /** The default version of Packer that will be used on
     * a supported platform if nothing else is configured.
     */
    static final String PACKER_DEFAULT = '1.1.1'

    /** Constructs a new extension which is attached to the provided project.
     *
     * @param project Project this extension is associated with.
     */
    PackerExtension(Project project) {
        super(project)
        if(Downloader.downloadSupported) {
            addVersionResolver(project)
            executable([ version : PACKER_DEFAULT ] as Map<String,Object>)
        } else {
            executable searchPath()
        }
    }

    /** Constructs a new extension which is attached to the provided task.
     *
     * @param project Project this extension is associated with.
     */
    PackerExtension(Task task) {
        super(task,NAME)
    }

    /** Resolves a path to a {@code packer} executable.
     *
     * @return Returns the path to the located {@code packer} executable.
     * @throw {@code GradleException} if executable was not configured.
     *
     * @deprecated Use {@code getResolvableExecutable()} instead.
     */
    @Deprecated
    ResolvableExecutable getResolvedPackerExecutable() {
        getResolvableExecutable()
    }

    /** Use this to configure a system path search for {@code Packer}.
     *
     * @return Returns a special option to be used in {@link #executable}
     */
    static Map<String,Object> searchPath() {
        PackerExtension.SEARCH_PATH
    }

    private void addVersionResolver(Project project) {

        ResolveExecutableByVersion.DownloaderFactory downloaderFactory = {
            Map<String, Object> options, String version,Project p ->
                new Downloader(version,p)
        } as ResolveExecutableByVersion.DownloaderFactory

        ResolveExecutableByVersion.DownloadedExecutable resolver = { Downloader installer ->
            installer.getPackerExecutablePath()
        } as ResolveExecutableByVersion.DownloadedExecutable

        getResolverFactoryRegistry().registerExecutableKeyActions(
            new ResolveExecutableByVersion(project,downloaderFactory,resolver)
        )
    }


    private static final Map<String,Object> SEARCH_PATH = [ search : 'packer' ] as Map<String,Object>

}
