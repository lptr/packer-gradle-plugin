//
// ============================================================================
// (C) Copyright Schalk W. Cronje 2017
//
// This software is licensed under the Apache License 2.0
// See http://www.apache.org/licenses/LICENSE-2.0 for license details
//
// Unless required by applicable law or agreed to in writing, software distributed under the License is
// distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and limitations under the License.
//
// ============================================================================
//

package org.ysb33r.gradle.packer.tasks

import org.gradle.api.Project
import org.gradle.process.ExecSpec
import org.gradle.testfixtures.ProjectBuilder
import org.ysb33r.gradle.packer.PackerExecSpec
import org.ysb33r.gradle.packer.PackerExtension
import spock.lang.Specification

class PackerBuilderSpec extends Specification {

    Project project = ProjectBuilder.builder().build()

    void 'Create PackerBuilder task'() {

        when: 'The packer plugin is applied'
        project.apply plugin : 'org.ysb33r.packer'

        then: 'A PackerBuilder task is created'
        project.tasks.getByName('packerBuild')

        when: 'The task is inspected'
        PackerBuilder task = project.tasks.getByName('packerBuild')

        then: 'A packer extensions is added to the task'
        task.extensions.getByName('packer') instanceof PackerExtension

        and:
        task.force == false
        task.parallel == true
        task.excludes.empty
        task.includes.empty

        when: 'Configuring the task'
        project.allprojects {
            // tag::configure-packer-task[]
            packerBuild {
                includes = [ 'box1' ]   // <1>
                includes 'box2', 'box3' // <2>
                excludes = [ 'box4' ]   // <3>
                excludes 'box5', 'box6' // <4>

                force true              // <5>
                parallel false          // <6>

                template  '/path/to/spec.json' // <7>
                outputDir "${buildDir}/work"   // <8>

                vars aws_access_key : 'MY_ACCESS_KEY',
                    aws_secret_key : { 'MY SECRET KEY' } // <9>
            }
            // end::configure-packer-task[]

            // tag::configure-environment[]
            packerBuild {
                setEnvironment System.getenv() // <1>

                environment foo :  'bar' // <2>
            }
            // end::configure-environment[]
        }

        then: 'The task attributes are set'
        task.includes.containsAll(['box1','box2','box3'])
        task.excludes.containsAll(['box4','box5','box6'])
        task.force == true
        task.parallel == false
        task.template == project.file('/path/to/spec.json')
        task.outputDir == project.file("${project.buildDir}/work")
        task.vars['aws_access_key'] == 'MY_ACCESS_KEY'
        task.vars['aws_secret_key'] == 'MY SECRET KEY'
        task.environment['foo'] == 'bar'

        when: 'The packer file location is configured via the task extension'
        project.allprojects {
            // tag::configure-packer-extension[]
            packerBuild {
                packer {
                    executable version : '1.0.0' // <1>
                }
            }
            // end::configure-packer-extension[]
        }

        then: 'The value in the project extension is no longer used'
        ((PackerExtension)project.packer).resolvedPackerExecutable != task.packer.resolvedPackerExecutable
    }

    void 'Variables must be passed correctly'() {
        setup:
        project.apply plugin : 'org.ysb33r.packer.base'
        String dummyPacker = new File('/path/to/packer').absolutePath

        project.allprojects {
            packer {
                executable path : dummyPacker
            }
        }
        PackerBuilder task = project.tasks.create('builder',PackerBuilder)
        task.outputDir = new File('foo').absoluteFile

        task.vars foo : 'ba r'
        task.template 'foo.json'

        when:
        PackerExecSpec execSpec = task.buildExecSpec()

        then:
        execSpec.commandLine == [
            dummyPacker,
            'build',
            '-var',
            "foo=ba r",
            new File(project.projectDir,'foo.json').absolutePath
        ]

    }
}