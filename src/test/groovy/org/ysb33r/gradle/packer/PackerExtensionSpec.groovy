//
// ============================================================================
// (C) Copyright Schalk W. Cronje 2017
//
// This software is licensed under the Apache License 2.0
// See http://www.apache.org/licenses/LICENSE-2.0 for license details
//
// Unless required by applicable law or agreed to in writing, software distributed under the License is
// distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and limitations under the License.
//
// ============================================================================
//

package org.ysb33r.gradle.packer

import org.gradle.api.GradleException
import org.gradle.api.Project
import org.gradle.testfixtures.ProjectBuilder
import spock.lang.Specification


class PackerExtensionSpec extends Specification {

    Project project = ProjectBuilder.builder().build()

    def 'Configure packer executable using a version'() {

        when: 'A version is configured'
        project.allprojects {

            apply plugin : 'org.ysb33r.packer.base'

            // tag::configure-with-tag[]
            packer {
                executable version : '1.0.3' // <1>
            }
            // end::configure-with-tag[]
        }

        then:
        project.packer.getResolvedPackerExecutable() != null
    }

    def 'Configure packer executable using a path'() {

        when: 'A path is configured'
        project.allprojects {

            apply plugin : 'org.ysb33r.packer.base'

            // tag::configure-with-path[]
            packer {
                executable path : '/path/to/packer' // <2>
            }
            // end::configure-with-path[]
        }

        then:
        project.packer.getResolvedPackerExecutable() != null
    }

    def 'Configure packer executable using a search path'() {

        when: 'A search is configured'
        project.allprojects {

            apply plugin : 'org.ysb33r.packer.base'

            // tag::configure-with-search-path[]
            packer {
                executable searchPath() // <3>
            }
            // end::configure-with-search-path[]
        }

        then:
        project.packer.getResolvedPackerExecutable() != null
    }

    def 'Cannot configure packer with more than one option'() {

        setup:
        project.apply plugin : 'org.ysb33r.packer.base'

        when:
        project.packer.executable version : '7.10.0', path : '/path/to'

        then:
        thrown(GradleException)

        when:
        project.packer.executable version : '7.10.0', search : '/path/to'

        then:
        thrown(GradleException)

    }
}