[[PackerExtension]]
== Packer Extension

The `packer` project extension configures a method of obtaining a `packer` executable. On <<platforms,supported platforms>> it offers the option of configuring a Packer version, which Gradle will then download, cache and use.

[source,groovy]
----
include::{testdir}/PackerExtensionSpec.groovy[tags=configure-with-tag,indent=0]

include::{testdir}/PackerExtensionSpec.groovy[tags=configure-with-path,indent=0]

include::{testdir}/PackerExtensionSpec.groovy[tags=configure-with-search-path,indent=0]
----
<1> Set a version. On <<platforms,supported platforms>> Gradle will download, cache and use this version.
<2> Specify a hard-coded path where to find the `packer` executable.
<3> Tell Gradle to search the system path for the `packer` executable.
