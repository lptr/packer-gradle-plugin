== The PackerBuilder task type

The base plugin (`org.ysb33r.packer.base`) makes a `PackerBuilder` task type available. If the the Packer plugin (`org.ysb33r.packer`) is applied it will create a task of this type called `packerBuild`.

[source,groovy]
----
include::{testdir}/tasks/PackerBuilderSpec.groovy[tags=configure-packer-task,indent=0]
----
<1> Replace current set of images to include with a new set.
<2> Add to current set of included images.
<3> Replace current set of images excludes with a new set.
<4> Add more images to be excluded.
<5> Force the images to be rebuilt. (`false` by default unless `--rerun-tasks` was specified on the command-line).
<6> Build images in parallel (`true` by default).
<7> The template input file to use.
<8> A working directory to which content may be output. Even if no output is expected, Packer will be executed with this as the working directory. the default is `${buildDir}/packer`.
<9> Add variables that will be passed to the `packer` process. Equivalent of `-var` on the command-line. Lazy evaluation is supported for the property values.

NOTE: By default nothing is excluded and everything is included.

The task type also adds `packer` task extension so that global configuration that was set in the <<PackerExtension,project extension>> called `packer` can be overriden on a per-task basis.

[source,groovy]
----
include::{testdir}/tasks/PackerBuilderSpec.groovy[tags=configure-packer-extension,indent=0]
----
<1> Override the version of Packer from the global setting

If might also be necessary to set the environment for `packer`. For instance if you are building a `virtualbox-ovf` image, you might need to locate `VBoxManage` on the system path.

[source,groovy]
----
include::{testdir}/tasks/PackerBuilderSpec.groovy[tags=configure-environment,indent=0]
----
<1> Reset the environment to inherit all of the Gradle system environment.
<2> Add an additional environmental variable.

NOTE: By default the environment is empty.