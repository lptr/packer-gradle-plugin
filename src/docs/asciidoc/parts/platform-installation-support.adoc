[[platforms]]
== Platform installation support

These plugins can automatically download, cache and render Node for the following platforms:

* Linux 32 & 64-bit.
* Mac 64-bit.
* Windows 32 & 64-bit.
* FreeBSD 32 & 64-bit.

Should you need to run Gradle on a platform not listed above, but which Node supports and on which Gradle can run, you will need to configure the Node executable via the `path` or `search` methods. You can also https://gitgub.com/ysb33r/packer-gradle-plugin[raise an issue] to ask for the support or you can submit a PR with the solution.